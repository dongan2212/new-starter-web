import { Injectable } from '@angular/core';

@Injectable()
export class LocalstorageService {

  constructor() { }

  saveUser(name: any){
    localStorage.setItem('currentUser',name);
  }

  deleteUser(){
    localStorage.removeItem('currentUser');
  }

}
