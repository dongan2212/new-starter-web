import { Injectable } from '@angular/core';
import {Http, Headers, Response, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import { MessageService } from './message.service';
import { Account } from '../model/account';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';

const httpOptions = {
  headers: new Headers({ 'Content-Type': 'application/json' })
};
@Injectable()
export class AuthenticationService {
  private authenticationUrl = environment.baseUrl;
  constructor(private http: Http,
              private messageService: MessageService) { }

  loginNormal(data: any): Observable<Account> {
    const options = new RequestOptions({headers: httpOptions.headers});
    console.log(data)
    return this.http.post(this.authenticationUrl+ `/api/login`, data, options).map((res: Response) => res.json() ).pipe(
          tap(_ => console.log('login')),
          catchError(this.handleError<Account>('login'))
        );
  }

  signup(data: any): Observable<Account> {
    const options = new RequestOptions({headers: httpOptions.headers});
    console.log(data)
    return this.http.post(this.authenticationUrl+ `/api/signup`, data, options).map((res: Response) => res.json() ).pipe(
          tap(_ => console.log('sign up')),
          catchError(this.handleError<Account>('signup'))
        );
  }
 
  IsLogged(): boolean {
    if (localStorage.getItem('currentUser')) {
      return true
    } else {
      return false
    }
}

logout(): void {
  localStorage.removeItem('currentUser');
}

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    this.messageService.add('Authentication service: ' + message);
  }
}
