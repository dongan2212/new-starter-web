import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../service/authentication.service';
import { LocalstorageService } from '../service/localstorage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  username: string;
  password: string;
  email: string;

  constructor(private authentication: AuthenticationService, private localstorageService: LocalstorageService
    ,private router: Router) { }

  ngOnInit() {

  }

  signup(){
    const data = JSON.stringify({
    'username': this.username,
    'email': this.email,
    'password': this.password});
    this.authentication.signup(data).subscribe(result =>{
        console.log(result)
        if(result.email != ''){
          this.localstorageService.saveUser(result.email)
          this.router.navigate(['']);
        }
    });
  }

}
