import {CanActivate} from '@angular/router';
import {Injectable} from '@angular/core';
import {AuthenticationService} from './service/authentication.service'


@Injectable()
export class AuthenticationGuard implements CanActivate {
    constructor(private authenticationService: AuthenticationService) {
    }
  
    canActivate() {
      return this.authenticationService.IsLogged();
    }
  }
  