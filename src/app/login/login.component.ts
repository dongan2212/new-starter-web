import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../service/authentication.service';
import {Account} from '../model/account';
import {Router} from '@angular/router';
import { LocalstorageService } from '../service/localstorage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  
  constructor(private authenticationService: AuthenticationService,private localstorageService: LocalstorageService
  ,private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    console.log('Click : ' + this.username + ' - ' + this.password);
    const body = JSON.stringify({'username': this.username,
  'password': this.password});
    this.authenticationService.loginNormal(body).subscribe(result => {
      console.log(result)
      if(result.email != ''){
        this.localstorageService.saveUser(result.email)
        this.router.navigate(['']);
      }
    })
  }
}
