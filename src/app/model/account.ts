export class Account {
  constructor(
    public accountId: number,
    public email: string,
    public password: string,
    public username: string,
    public roleId: number
  ) {  }

}
