import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import {HttpModule} from '@angular/http';
import {MessageService} from './service/message.service';
import {AuthenticationService} from './service/authentication.service';
import { SignupComponent } from './signup/signup.component';
import { AuthenticationGuard } from './AuthenticationGuard';
import { LocalstorageService } from './service/localstorage.service';
import { HomeComponent } from './home/home.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule
  ],
  providers: [MessageService, AuthenticationService, AuthenticationGuard, LocalstorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
